# Quiz

Small python script to ask multiple choice questions. Made initially to try and remember Japanese judo terms. Configuration can be placed in an ini file. The location of this file can be specified by the environment variable QUIZ_INI. If this isn't set the app will default to `quiz.ini` in the current directory. A sample `quiz.ini.sample` is included.
