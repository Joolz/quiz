#!/usr/bin/python3
"""
Scriptlet to read a csv file QUESTIONS containing

label;correct_answer;level

and make multiple choice questions of it. Enter the option
of your choise, q to quit, enter to go to the next question.
"""

import csv
import random
import configparser
import sys
import os
import datetime

config = configparser.ConfigParser()

try:
    INI = os.environ["QUIZ_INI"]
except KeyError:
    INI = "quiz.ini"

if os.path.exists(INI):
    config.read(INI)
else:
    print("Configuration not found, see README.md")
    sys.exit(1)

QUESTIONS: str = config.get("DEFAULT", "questions")
DELIMITER: str = config.get("DEFAULT", "delimiter")
LANGUAGE: str = config.get("DEFAULT", "language")
LEVELSFILE: str = "levels.csv"
LEVEL_COLUMN: int = 0

try:
    MAX_OPTIONS: int = config.getint("DEFAULT", "max_options")
    LEVEL: int = config.getint("DEFAULT", "level")

    # If this is set to True, only questions at LEVEL will be used.
    # If it's False, levels up to and including LEVEL will be used.
    LEVEL_EXCLUSIVE: bool = config.getboolean("DEFAULT", "level_exclusive")

    # Left to right means: take column one as the question, column two
    # as the options column. If it is set to False, column two will be
    # the question column and column one the options column.
    LEFT_TO_RIGHT: bool = config.getboolean("DEFAULT", "left_to_right")
    SHOW_LEVEL: bool = config.getboolean("DEFAULT", "show_level")
except ValueError as error:
    print(error)
    sys.exit(1)

QUESTION_COLUMN: int = 1 if LEFT_TO_RIGHT else 2
OPTION_COLUMN: int = 2 if LEFT_TO_RIGHT else 1

translationfile = configparser.ConfigParser()
translationfile.read("quiz.translations")
translations = dict(translationfile.items(LANGUAGE))

MAX_LOOPS: int = 25
all_options = []

# list of incorrectly answered questions, so we can offer a retry run
incorrectly_anwered = []


class Quiz:
    """A quiz is a list of questions with their options"""

    def __init__(self):
        self._questions = []
        self._levels = {}
        self.start_time = None
        self.is_started = False
        self.duration = None

        with open(QUESTIONS, "r", encoding="UTF8") as questions_file:
            for line in csv.reader(questions_file, delimiter=DELIMITER):
                self._questions.append(
                    Question(
                        line[QUESTION_COLUMN], line[OPTION_COLUMN], line[LEVEL_COLUMN]
                    )
                )

        if os.path.exists(LEVELSFILE):
            try:
                with open(LEVELSFILE, "r", encoding="UTF8") as levels_file:
                    for line in csv.reader(levels_file, delimiter=DELIMITER):
                        self._levels[int(line[0])] = line[1]
            except UnicodeDecodeError as uerror:
                print(uerror)
        else:
            print(f"{LEVELSFILE} not found, exiting")
            sys.exit(1)

        self.filter_on_level()
        random.shuffle(self._questions)

    def start(self):
        """Start the quiz"""
        self.start_time = datetime.datetime.now().replace(microsecond=0)
        self.is_started = True

    def get_duration(self):
        """Get the duration so far"""
        now = datetime.datetime.now().replace(microsecond=0)
        duration_time = now - self.start_time
        hours, remainder = divmod(duration_time.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)
        result = ""

        if hours != 0:
            result += f"{int(hours)}h "

        if minutes != 0:
            result += f"{int(minutes)}m "

        result += f"{int(seconds)}s"

        return result

    def get_level_description(self, level):
        """Get the level description of a question"""
        return self._levels[level]

    def number_of_questions(self):
        """Return the number of questions"""
        return len(self._questions)

    def filter_on_level(self):
        """Filter on level"""
        if LEVEL_EXCLUSIVE:
            self._questions = [q for q in self._questions if q.level == LEVEL]
        else:
            self._questions = [q for q in self._questions if q.level <= LEVEL]

    def get_question(self):
        """Get a question and remove it from the list"""
        return None if len(self._questions) == 0 else self._questions.pop(0)

    def set_questions(self, questions):
        """Set the questions of a quiz. Handy for retry runs"""
        self._questions = questions

    def __len__(self):
        return len(self._questions)

    def __getitem__(self, position):
        return self._questions[position]


class Question:
    """Question class"""

    def __init__(self, text, correct_option, level):
        self.text: str = text
        self.add_options(correct_option)
        self.level: int = int(level)
        self.is_answered_correctly = False

    def add_options(self, correct_option):
        """Add question options, avoid duplicates"""

        self.options = []
        self.options.append(Option(correct_option, True))

        attempt = 0
        while len(self.options) < MAX_OPTIONS:
            randint = random.randint(0, len(all_options) - 1)
            candidate = all_options[randint]
            candidate.correct = False

            exists = False
            for option in self.options:
                if candidate.text == option.text:
                    exists = True
                    break

            if not exists:
                self.options.append(candidate)

            if attempt > MAX_LOOPS:
                print(f"{trans('loops_exhausted')}")
                sys.exit(1)
            else:
                attempt += 1

        random.shuffle(self.options)
        option_number = "a"
        for option in self.options:
            option.number = option_number
            option_number = chr(ord(option_number) + 1)

    def get_correct_answer(self):
        """Return the correct answertext of the question"""
        for option_ in self.options:
            if option_.correct:
                return option_.text

    def is_correct_answer(self, check):
        """Check is a given text is the correct answer"""
        correct = check == self.get_correct_answer()
        if correct:
            self.is_answered_correctly = True
        else:
            incorrectly_anwered.append(
                Question(self.text, self.get_correct_answer(), self.level)
            )
        return correct

    def get_level(self):
        """Return the level of the question"""
        return self.level

    def read_input(self, option_lines):
        """Check if input is valid"""
        valid_options = []
        for option_line in option_lines:
            valid_options.append(option_line.split(" ")[0])
        valid_options.append("q")

        result = input("")
        while result not in valid_options:
            print(f"{trans('choose_a_valid_option')} {valid_options}")
            result = input("")

        return result

    def ask(self, question_number, total, level_description):
        """Ask a question. Return True or False for correctness, None if user wants to quit"""

        number_text = f"{str(question_number)}/{total}"
        if SHOW_LEVEL and not LEVEL_EXCLUSIVE:
            number_text += f" ({level_description})"

        message: str = f"{number_text}. {trans('meaning_of')} {self.text}?"

        print("")
        print("-" * len(message))
        print(message)

        option_lines = []
        option_line_number = "a"
        for option in self.options:
            option_lines.append(f"{option_line_number} {option.text}")
            option_line_number = chr(ord(option_line_number) + 1)

        while True:
            chosen = self.choose_option(option_lines)

            if chosen is None:
                return None

            if chosen == -1:
                print(trans("invalid_coice"))
            else:
                break

        correct = self.is_correct_answer(chosen)
        if correct:
            print(f"\"{chosen}\" {trans('is_correct')}")
        else:
            print(
                f"\"{chosen}\" {trans('is_incorrect')} \"{self.get_correct_answer()}\""
            )

        return correct

    def choose_option(self, option_lines):
        """Choose one option or quit"""
        for option_line in option_lines:
            print(f"  {option_line}")
        choice = self.read_input(option_lines)

        if choice == "q":
            return None

        # pick the response starting with choice
        for option_line in option_lines:
            chosen: int = -1
            if option_line.startswith(f"{choice} "):
                chosen = option_line[2 : len(option_line)]
                break
        return chosen


class Option:
    """A possible answer option"""

    def __init__(self, text, correct_):
        self.number = ""
        self.text = text
        self.correct = correct_


def trans(key):
    """Give translation for a key"""
    ret = translations[key]
    return ret


def main():
    """Main method"""

    with open(QUESTIONS, "r", encoding="UTF8") as questions_file:
        for line in csv.reader(questions_file, delimiter=DELIMITER):
            all_options.append(Option(line[OPTION_COLUMN], False))

    quiz = Quiz()
    run = 1

    if quiz.number_of_questions() == 0:
        print(f"{trans('no_questions')}. {trans('check_config')}.")

    while quiz.number_of_questions() > 0:
        total = quiz.number_of_questions()
        question_number: int = 1
        correct: int = 0

        welcome: str = f"{quiz.number_of_questions()} {trans('questions')}"
        if LEVEL_EXCLUSIVE:
            welcome += f" {trans('using_level')} {quiz.get_level_description(LEVEL)}"

        welcome += f". {trans('explanation')}, run {run}"

        print("")
        print("*" * len(welcome))
        print(welcome)

        question = quiz.get_question()
        while question is not None:
            is_correct = question.ask(
                question_number,
                total,
                quiz.get_level_description(question.level),
            )

            if not quiz.is_started:
                quiz.start()

            if is_correct is None:
                incorrectly_anwered.clear()  # prevent q new run if we want to quit
                break
            if is_correct:
                correct += 1
            question_number += 1
            question = quiz.get_question()

        if question_number == 0 or correct == 0:
            result_text = trans("all_wrong")
        else:
            result_text = f"{trans('correct')}: {round(correct / (question_number - 1) * 100, 2)}%"
            result_text += f" ({correct}/{question_number-1}) {trans('in_run')} {run}"

        result_text += f" {trans('completed_in')} {quiz.get_duration()}"

        print("=" * len(result_text))
        print(result_text)
        print("=" * len(result_text))

        run += 1
        quiz.set_questions(incorrectly_anwered.copy())
        incorrectly_anwered.clear()


if __name__ == "__main__":
    main()
